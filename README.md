# dns 101
## Terminologia

* DNS ou Domain Name System é um dos pilares fundamentais da internet

* Como se fosse uma lista de contatos de seu smartfone. Associa-se um domínio a um endereço ip.

```
➜  ~ ping -c2 google.com
PING google.com (172.217.29.142) 56(84) bytes of data.
64 bytes from gru10s01-in-f142.1e100.net (172.217.29.142): icmp_seq=1 ttl=56 time=5.41 ms
64 bytes from gru10s01-in-f142.1e100.net (172.217.29.142): icmp_seq=2 ttl=56 time=9.71 ms
```

## Importância:

* Web browsers utlizam IP endereços IP's (Internet Protocol) para conectar em sites

* Elimina necessidade de decorar o endereço ip de sites, endpoints

## História

* Um arquivo "super" hosts.txt era utilizado para mapeamento de todos os hosts na [arpanet](https://sites.google.com/site/sitesrecord/o-que-e-arpanet)
* Update era semanal
* Consulta ao arquivo era linear
* consistência

## Hierarquia

1. Root Level
* Resolve o host em um endereço ip

2. Top Level Domain Nameservers
* O domínio de topo (sigla: TLD, do inglês top-level domain), é um dos componentes dos endereços de Internet. Cada nome de domínio na Internet consiste de alguns nomes separados por pontos, e o último desses nomes é o domínio de topo. 
Por exemplo, no nome de domínio exemplo.com, o TLD é "com"

* OS TLD podem ser:

    * Generic Top Level Domain (gTLD) : example edu, com, net, and mil
    * Country Code Top Level Domain(ccTLD) : Por exemplo us = USA

3. Second Level Domains
* O equivalente ao "example" acima citado

4. Sub-Domain
* considerando a entrada test.example.com, seria "test"

## Diagrama
![hierarquia](_aux/img/dns.png)

## Como uma consulta funciona?

![dns query](_aux/img/dns2.png)

## Entrada dns

Entradas DNS são registros configurados na zona DNS de um determinado domínio, com instruções para resolver o nome de um domínio especifico de internet. 

Os comumentes utilizados são:

* A - Address: Apontam diretamente para um endereço IP.

* CNAME - Canonical Name: Apontam para outro endereço (por exemplo, dominio.com.br). O servidor responsável pela hospedagem do endereço deve ser configurado para identificar as solicitações das entradas CNAME.

* MX - Mail Exchange: Apontam para o servidor de e-mail que responde pelo domínio.

* NS - Name Server: Indicam os servidores DNS que respondem pelo domínio.

* TXT: São utilizadas nas configurações de e-mails (por exemplo, para políticas anti-spam).

* SOA - Start of Authority: Definem as características de uma Zona de DNS.

* SRV: Definem as localizações de serviços publicados, portas e protocolos.

## Troubleshooting

# dig

![dig meme](_aux/img/dig.png)


O comando dig é uma ferramenta para consultar servidores DNS e obter informações sobre endereços de host, trocas de correio, servidores de nomes e informações relacionadas. Essa ferramenta pode ser usada em qualquer sistema operacional Linux (Unix) ou Macintosh OS X.

Obtendo endereço ip da entrada tipo A
```
dig redhat.com A
```

Informações sobre servidor de email
```
dig yahoo.com MX
```

Informações sobre o name server do domínio
```
dig fedora.com NS
```

Obtendo o ttl
```
dig usa.gov TTL
```

Entradas txt
```
dig usa.gov TXT
```

Consulta a partir de outro dns server
```
dig usa.gov +stats
dig @8.8.8.8 usa.gov +stats
```

Multiplas consultas
```
dig ubuntu.org fedora.org manjaro.com
```

# whois

Consulta pública de dominios br.

https://registro.br/tecnologia/ferramentas/whois/

![consulta whois](_aux/img/whois.png)

# ipok

Análise completa de dominios, consulta de dns reverso, etc

https://ipok.com.br/

![consulta ipok](_aux/img/ipok.png)

## Conclusão

Entender dns é fundamental para qualquer pessoa, não se restringindo a TI, pois com conhecimento básico sobre, é possível mitigar possíveis fraudes,
redirecinamento maliciosos,  otimizar e navegar na internet de forma mais segura.

## Referências

https://www.cloudflare.com/learning/dns/what-is-dns/  
https://medium.com/tech-tajawal/journey-to-dns-part-1-96348198a2be  
https://www.howtogeek.com/663056/how-to-use-the-dig-command-on-linux/  
https://medium.com/@kamranahmedse/dns-in-one-picture-d7f4783db06a  
